import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    
    {
        id      : 'pages',
        title   : 'Dashboard',
        type    : 'group',
        icon    : 'pages',
        children: [
            {
                id      : 'Home',
                title   : 'Home',
                type    : 'item',
                icon    : 'home',
                url  : '/pages/home'
            },
            {
                id      : 'pedidos',
                title   : 'Pedidos',
                type    : 'collapsable',
                icon    : 'assignment_turned_in',
                children: [
                    {
                        id   : 'nuevo-pedido',
                        title: 'Nuevo pedido',
                        type : 'item',
                        url  : '/pages/pedidos/nuevo-pedido'
                    },
                    {
                        id   : 'pedido-transcurso',
                        title: 'En Transcurso',
                        type : 'item',
                        url  : '/pages/pedidos/pedido-transcurso'
                    },
                    {
                        id   : 'historial',
                        title: 'Historial',
                        type : 'item',
                        url  : '/pages/pedidos/historial'
                    }
                ]
            },
            {
                id      : 'pagos',
                title   : 'Formas de Pago',
                type    : 'collapsable',
                icon    : 'local_atm',
                url  : '/pages/pagos',
                children: [
                    {
                        id   : 'nueva-forma',
                        title: 'Agrega Nueva Forma',
                        type : 'item',
                        url  : '/pages/pagos/nueva-forma'
                    },
                    {
                        id   : 'editar-forma',
                        title: 'Edita Nueva Forma',
                        type : 'item',
                        url  : '/pages/pagos/editar-forma'
                    }
                ]
            },
            {
                id      : 'cupones',
                title   : 'Cupones',
                type    : 'collapsable',
                icon    : 'local_play',
                children: [
                    {
                        id   : 'cupones-activos',
                        title: 'Cupones Activos',
                        type : 'item',
                        url  : '/pages/cupones/cupones-activos'
                    },
                    {
                        id   : 'agregar-cupon',
                        title: 'Agregar Cupón',
                        type : 'item',
                        url  : '/pages/cupones/agregar-cupon'
                    }
                ]
            },
            {
                id      : 'cuenta-premium',
                title   : 'Cuenta Premium',
                type    : 'item',
                icon    : 'portrait',
                url  : '/pages/cuenta-premium'
            },
            


            

            
        ]
    }

    
];
