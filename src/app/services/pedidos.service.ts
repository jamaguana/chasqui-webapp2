import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SERVER_URL } from '../api';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { JwtHelperService } from '@auth0/angular-jwt';

const headers = new HttpHeaders().set("Authorization", "Bearer "+localStorage.getItem('access_token') );

@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  constructor(
    public http: HttpClient,
    public ls:LocalStorage,
    public jwtHelper: JwtHelperService
  ) { 

  }


  productos(){
    return new Promise((respueta, errores) => {

      this.http.get( SERVER_URL+'/get_products', { headers })
        .subscribe(res => {
          console.log('PRODUCTOS:',res)
          if(res['response']){
            respueta( res['response'] );
          }else{
            respueta( undefined );
          }

        }, (err) => {
          if(err.error){
            errores(err.error.error);
          }else{
            errores(false);
          }
        });
    });
    
  }


  historial(){
    return new Promise((respueta, errores) => {

      this.http.get( SERVER_URL+'/myHistoryOrders/20', { headers })
        .subscribe(res => {
          console.log('HISTORIAL:',res)
          if(res['response']){
            respueta( res['response'] );
          }else{
            respueta( undefined );
          }

        }, (err) => {
          if(err.error){
            errores(err.error.error);
          }else{
            errores(false);
          }
        });
    });
  }
  


}
