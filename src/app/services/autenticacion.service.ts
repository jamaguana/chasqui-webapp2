import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVER_URL } from '../api';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {

  constructor(
    public http: HttpClient,
    public ls:LocalStorage,
    public jwtHelper: JwtHelperService
  ) { 

  }

  checkLogin(){
    if( this.jwtHelper.isTokenExpired() ){
      return true;
    }else{
      return false;
    }
  }


  login( form ){
    return new Promise((respueta, errores) => {
 
      this.http.post( SERVER_URL+'/login', form)
        .subscribe(res => {
          console.log(res)
          if(res['response']){
            localStorage.setItem('access_token',res['response']['token']);
          }
          respueta( res);

        }, (err) => {
          if(err.error){
            errores(err.error.error);
          }else{
            errores(false);
          }
          
        });
    });
  }

}
