import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SERVER_URL } from '../api';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { JwtHelperService } from '@auth0/angular-jwt';

const headers = new HttpHeaders().set("Authorization", "Bearer "+localStorage.getItem('access_token') );

@Injectable({
  providedIn: 'root'
})
export class CuponesService {

  constructor(
    public http: HttpClient,
    public ls:LocalStorage,
    public jwtHelper: JwtHelperService
  ) { 

  }


  cupones(){
    return new Promise((respueta, errores) => {

      this.http.get( SERVER_URL+'/get_cupons', { headers })
        .subscribe(res => {
          console.log('CUPONES:',res)
          if(res['response']){
            respueta( res['response'] );
          }else{
            respueta( undefined );
          }

        }, (err) => {
          if(err.error){
            errores(err.error.error);
          }else{
            errores(false);
          }
        });
    });
  }

  insertar( form ){
    return new Promise((respueta, errores) => {

      this.http.post( SERVER_URL+'/use_cupon',form ,{ headers })
        .subscribe(res => {
          console.log('AGREGAR CUPONES:',res)

          if(res['error']){
            respueta(res['error']);return;
          }

          if(res['response']){
            respueta( res['response'] );
          }


        }, (err) => {
          if(err.error){
            errores(err.error.error);
          }else{
            errores(false);
          }
        });
    });

  }


}
