import { TestBed } from '@angular/core/testing';

import { AuthHttpServiceService } from './auth-http-service.service';

describe('AuthHttpServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthHttpServiceService = TestBed.get(AuthHttpServiceService);
    expect(service).toBeTruthy();
  });
});
