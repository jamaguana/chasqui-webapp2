
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { NuevaFormaComponent } from './nueva-forma.component';

const routes = [
    {
        path     : 'pagos/nueva-forma',
        component: NuevaFormaComponent
    }
];

@NgModule({
    declarations: [
        NuevaFormaComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule
    ],
    exports     : [
        NuevaFormaComponent
    ]
})

export class NuevaFormaModule
{
}
