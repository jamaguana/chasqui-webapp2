import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditarFormaComponent } from './editar-forma.component';

const routes = [
    {
        path     : 'pagos/editar-forma',
        component: EditarFormaComponent
    }
];

@NgModule({
    declarations: [
        EditarFormaComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule
    ],
    exports     : [
        EditarFormaComponent
    ]
})

export class EditarFormaModule
{
}
