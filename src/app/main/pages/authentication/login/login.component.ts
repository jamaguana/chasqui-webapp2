import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AutenticacionService } from '../../../../services/autenticacion.service';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorage } from '@ngx-pwa/local-storage';

@Component({
    selector     : 'login',
    templateUrl  : './login.component.html',
    styleUrls    : ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class LoginComponent implements OnInit
{
    loginForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        public servicio: AutenticacionService,
        public snackBar: MatSnackBar,
        private route: ActivatedRoute,
        private router: Router,
        protected localStorage: LocalStorage
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        console.log('LOGIN EXPIRO:', this.servicio.checkLogin() );

        if( !this.servicio.checkLogin() ){
            this.router.navigate(['pages/home']);
        }

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.loginForm = this._formBuilder.group({
            email   : ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }


    iniciarSesion(form){
        this.servicio.login(form).then((result) => {

            this.localStorage.setItem('ls_usuario', result['response']['user'] ).subscribe(() => {
                location.reload();
            });
            //this.router.navigate(['pages/home']);
            
        }, (err) => {
            if(err){
                this.mensaje( 'Usuario o contraseña incorrectos.','Aceptar' );
            }else{
                this.mensaje( 'Lo sentimos, error desconocido.','Aceptar' );
            }
        });
    }


    mensaje(texto: string, boton: string){
        this.snackBar.open(texto, boton, {
            duration: 2000,
        });
    }


}
