import { Component } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';


@Component({
    selector   : 'cuenta-premium',
    templateUrl: './cuenta-premium.component.html',
    styleUrls  : ['./cuenta-premium.component.scss']
})
export class CuentaPremiumComponent
{
    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    )
    {
        
    }
}
