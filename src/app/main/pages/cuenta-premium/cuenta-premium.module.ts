import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { CuentaPremiumComponent } from './cuenta-premium.component';

const routes = [
    {
        path     : 'cuenta-premium',
        component: CuentaPremiumComponent
    }
];

@NgModule({
    declarations: [
        CuentaPremiumComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule
    ],
    exports     : [
        CuentaPremiumComponent
    ]
})

export class CuentaPremiumModule
{
}
