import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { MapaModalComponent } from './mapa-modal.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { MatButtonModule } from '@angular/material/button';

const routes = [
    {
        path     : 'mapa-modal/mapa-modal',
        component: MapaModalComponent
    }
];

@NgModule({
    declarations: [
        MapaModalComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule,
        GooglePlaceModule,
        MatButtonModule
    ],
    exports     : [
        MapaModalComponent
    ],
})

export class MapaModalModule
{
}
