import { Component, OnInit,Inject,ElementRef,ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";

declare var google;
declare var $;

export interface DialogData {
  LatLng : any;
  id: number;
  direccion: string;
}

@Component({
  selector: 'app-mapa-modal',
  templateUrl: './mapa-modal.component.html',
  styleUrls: ['./mapa-modal.component.scss']
})
export class MapaModalComponent implements OnInit {

  @ViewChild('map_canvas') mapElement: ElementRef;
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  map: any;
  options:any;
  
  constructor(
    private dialogRef: MatDialogRef<MapaModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) 
  {

    this.options = {
      types: [],
      componentRestrictions: { country: 'EC' }
    }
    
  }

  ngOnInit() {
    
    this.buscarme();

  }


  public handleAddressChange(address) {
    //console.log( address );
    let dir = address;
    let myLatLng = {lat: dir.geometry.location.lat(), lng: dir.geometry.location.lng() };
    this.actualizarMapa(myLatLng);
  }

  actualizarMapa( myLatLng ){
    var center = new google.maps.LatLng( myLatLng );
    this.map.panTo(center);
  }

  buscarme() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        //console.log(position)
        let myLatLng = {lat: position.coords.latitude , lng: position.coords.longitude };
        this.loadMap(myLatLng);
      });
    } else {
      alert("Geolocation is not supported by this browser.");
      let myLatLng = {lat: -0.2040359, lng: -78.4895438 };
      this.loadMap(myLatLng);
    }
  }

  loadMap(myLatLng){

    this.map = new google.maps.Map( this.mapElement.nativeElement , {
      center: myLatLng,
      zoom: 18
    });

    $('<div/>').addClass('centerMarker').appendTo(this.map.getDiv()).click(function(){
        var that=$(this);
        if(!that.data('win')){
        that.data('win',new google.maps.InfoWindow({content:'this is the center'}));
        that.data('win').bindTo('position',this.map,'center');
        }
        that.data('win').open(this.map);
    });

    /*
    let marker = new google.maps.Marker({
      position: myLatLng,
      title:  "Aquí me encuentro!"
    });
    marker.setMap(this.map);
    */

   //google.maps.event.addDomListener(window, 'load', this.loadMap);

  }


  aceptar(){
   
    let myLatLng = this.map.getCenter();
    //console.log(myLatLng.lat(),myLatLng.lng());

    myLatLng = {lat: myLatLng.lat(), lng: myLatLng.lng() };

    let  geocoder = new google.maps.Geocoder;


    let self = this;

    geocoder.geocode({'location': myLatLng}, function(results, status) {
      if (status === 'OK') {
        if (results[0]) {
          console.log("DIRECCION ", results[0].formatted_address );
          self.data.direccion = results[0].formatted_address;
          self.data.LatLng =  myLatLng;
    
          self.dialogRef.close(self.data);

        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
    
  }




}
