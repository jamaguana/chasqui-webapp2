import { Component } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { CuponesService } from '../../../../services/cupones.service';


@Component({
    selector   : 'cupones-activos',
    templateUrl: './cupones-activos.component.html',
    styleUrls  : ['./cupones-activos.component.scss']
})
export class CuponesActivosComponent
{
    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    cupones: any = [];
    displayedColumns:any;

    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        public servicio: CuponesService 
    )
    {
        this.getCupones();
        this.displayedColumns = ['name', 'description', 'code', 'value'];
    }


    getCupones(){
        this.servicio.cupones().then((result) => {
            this.cupones = result;
        }, (err) => {
            console.log('-->',err);
        });
    }

    


}
