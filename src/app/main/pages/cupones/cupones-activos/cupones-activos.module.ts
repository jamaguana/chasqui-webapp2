import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { CuponesActivosComponent } from './cupones-activos.component';
import {MatTableModule} from '@angular/material/table';

const routes = [
    {
        path     : 'cupones/cupones-activos',
        component: CuponesActivosComponent
    }
];

@NgModule({
    declarations: [
        CuponesActivosComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule,
        MatTableModule
    ],
    exports     : [
        CuponesActivosComponent
    ]
})

export class CuponesActivosModule
{
}
