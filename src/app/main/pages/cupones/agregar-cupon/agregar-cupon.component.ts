import { Component } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { CuponesService } from '../../../../services/cupones.service';
import { MatSnackBar } from '@angular/material';

@Component({
    selector   : 'agregar-cupon',
    templateUrl: './agregar-cupon.component.html',
    styleUrls  : ['./agregar-cupon.component.scss']
})
export class AgregarCuponComponent
{
    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    cupon: string;

    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        public servicio: CuponesService ,
        public snackBar: MatSnackBar,
    )
    {
        //this.getProductos();
        
    }


    insetarCupones(){

        if(!this.cupon){
            this.mensaje('Ingrese un cupon','Aceptar');
            return;
        }

        let form = {
            cupon: this.cupon
        }
        this.servicio.insertar( form ).then((result) => {
            console.log('----->', result );
            let texto = result.toString();
            this.mensaje( texto ,'Aceptar');
            this.cupon = undefined;

        }, (err) => {
            console.log('-->',err);
        });
    }


    mensaje(texto: string, boton: string){
        this.snackBar.open(texto, boton, {
            duration: 2000,
        });
    }

    


}
