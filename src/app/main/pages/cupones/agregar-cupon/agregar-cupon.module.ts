import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { AgregarCuponComponent } from './agregar-cupon.component';
import { MatCardModule } from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

const routes = [
    {
        path     : 'cupones/agregar-cupon',
        component: AgregarCuponComponent
    }
];

@NgModule({
    declarations: [
        AgregarCuponComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule
    ],
    exports     : [
        AgregarCuponComponent
    ]
})

export class AgregarCuponModule
{
}
