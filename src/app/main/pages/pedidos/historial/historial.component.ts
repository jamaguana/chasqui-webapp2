import { Component } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { PedidosService } from '../../../../services/pedidos.service';


@Component({
    selector   : 'historial',
    templateUrl: './historial.component.html',
    styleUrls  : ['./historial.component.scss']
})
export class HistorialComponent
{
    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    historial:any= [];
    displayedColumns:any;

    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        public servicio: PedidosService 
    )
    {
        this.getProductos();
        this.displayedColumns = ['nombre', 'duracion', 'precio', 'info'];
        
    }


    getProductos(){
        this.servicio.historial().then((result) => {
            console.log('----->', result );
            this.historial = result;
        }, (err) => {
            console.log('-->',err);
        });
    }


}
