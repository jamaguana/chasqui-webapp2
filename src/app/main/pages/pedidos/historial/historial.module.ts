import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { HistorialComponent } from './historial.component';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';

const routes = [
    {
        path     : 'pedidos/historial',
        component: HistorialComponent
    }
];

@NgModule({
    declarations: [
        HistorialComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule,
        MatListModule,
        MatCardModule,
        MatTableModule
    ],
    exports     : [
        HistorialComponent
    ]
})

export class HistorialModule
{
}
