import { Component } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { PedidosService } from '../../../../services/pedidos.service';


@Component({
    selector   : 'pedido-transcurso',
    templateUrl: './pedido-transcurso.component.html',
    styleUrls  : ['./pedido-transcurso.component.scss']
})
export class PedidoTranscursoComponent
{
    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        public servicio: PedidosService 
    )
    {
        this.getProductos();
        
    }


    getProductos(){
        this.servicio.productos().then((result) => {
            console.log('----->', result );
        }, (err) => {
            console.log('-->',err);
        });
    }


}
