import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { PedidoTranscursoComponent } from './pedido-transcurso.component';

const routes = [
    {
        path     : 'pedidos/pedido-transcurso',
        component: PedidoTranscursoComponent
    }
];

@NgModule({
    declarations: [
        PedidoTranscursoComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule
    ],
    exports     : [
        PedidoTranscursoComponent
    ]
})

export class PedidoTranscursoModule
{
}
