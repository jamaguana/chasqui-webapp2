import { Component,ViewChild } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { PedidosService } from '../../../../services/pedidos.service';
import { MatSnackBar } from '@angular/material';
import { MatDialog } from '@angular/material';
import { MapaModalComponent } from '../../mapa-modal/mapa-modal.component';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";

const inputs = { 
    value : undefined,
    LatLng: {}   
};

@Component({
    selector   : 'nuevo-pedido',
    templateUrl: './nuevo-pedido.component.html',
    styleUrls  : ['./nuevo-pedido.component.scss']
})
export class NuevoPedidoComponent
{
    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */

    @ViewChild("placesRef") placesRef : GooglePlaceDirective;

    productos:any= [];
    cargando: boolean = true;
    
    paso:number = 1;
    inicio:any;
    direcciones: any = [];


    productosSeleccionados:any = [];
    options:any;

    
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        public servicio: PedidosService ,
        public snackBar: MatSnackBar,
        public dialog: MatDialog
    )
    {
        let temp = { id: undefined, cantidad: undefined };
        this.productosSeleccionados = temp;
        this.getProductos();
        this.direcciones.push( inputs );
        
        this.options = {
            types: [],
            componentRestrictions: { country: 'EC' }
        }

        this.inicio = { 
            value : undefined,
            LatLng: {}   
        };

    }

    public handleAddressChange(address) {
        console.log( address );
        let dir = address;
        console.log( dir.geometry.location.lat() );
    }


    getProductos(){
        this.servicio.productos().then((result) => {
            this.productos = result;
            
            for (let index = 0; index < this.productos.length; index++) {
                this.productos[index]['cantidad'] = 0;
            }

            this.cargando = false;
        }, (err) => {
            this.cargando = false;
            console.log('Error:',err);
        });
    }

    agregarUbicacion(){

        let temp = this.direcciones;
        
        if( this.direcciones.length == 4 ){
            this.mensaje('Solo puede agregar 4 direcciones','Aceptar');
            return;
        }
        temp.push( { 
            value : undefined,
            LatLng: {}   
        } );


        this.direcciones = temp;

        console.log( this.direcciones );
    }

    eliminar( index ){
        this.direcciones.splice( index, 1);
    }

    continuar( ){
        let incompletas = 0;

        for (let index = 0; index < this.productos.length; index++) {
            if( this.productos[index]['cantidad'] == 0){
                incompletas ++;
            }
        }

        if( incompletas == this.productos.length ){
            this.mensaje('Selecciona almenos un producto','Aceptar');
            return;
        }

        this.paso = 2;
    }

    volver(){
        if(this.paso > 1){
            this.paso = this.paso - 1;
        }
    }

    validar( id ){

        if( this.productos[id]['is_combo'] == true &&  this.productos[id]['cantidad'] > 0 ){

            for (let index = 0; index < this.productos.length; index++) {

                if( id != index ){
                    if( this.productos[index]['cantidad'] > 0 ){
                        this.productos[index]['cantidad'] = 0;
                        this.mensaje('No puedes seleccionar productos normales y combos','Aceptar');
                    }
                }
            }
        }

        if( this.productos[id]['is_combo'] == false &&  this.productos[id]['cantidad'] > 0 ){

            for (let index = 0; index < this.productos.length; index++) {

                if( this.productos[index]['is_combo'] == true ){

                    if( this.productos[index]['cantidad'] > 0 ){
                        this.productos[index]['cantidad'] = 0;
                        this.mensaje('No puedes seleccionar productos normales y combos','Aceptar');
                    }
                }
            }
        }
        
    }


    /**
     * Abrir el mapa para seleccionar las ubicaciones
     * @param id 
     */
    openDialog( id ): void {

        console.log('ID SELECCIONADO', id);

        const dialogRef = this.dialog.open(MapaModalComponent, {
          width: '50%',
          data: {LatLng: undefined, id: id, direccion: undefined },
          disableClose : true
        });
    
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed', result);

            if( result.id === false ){
                this.inicio.value =  result.direccion;
                this.inicio.LatLng =  result.LatLng;
                
            }else{
                this.direcciones[ result.id ]['value'] = result.direccion;
                this.direcciones[ result.id ]['LatLng'] = result.LatLng;
            }

            console.log('DIRECCIONES FINAL', this.direcciones);
        });

    }


    cotizar(){
        let faltantes = 0;
        for (let index = 0; index < this.direcciones.length; index++) {
            if( !this.direcciones[index]['value'] ){
                faltantes ++;
            }
        }

        if( !this.inicio.value ){
            this.mensaje('Complete la dirección de origen','Aceptar');
            return;
        }

        if(faltantes > 0){
            this.mensaje('Complete las direcciones de destino','Aceptar');
            return;
        }

        

    }



    mensaje(texto: string, boton: string){
        this.snackBar.open(texto, boton, {
            duration: 2000,
        });
    }


}
