import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { NuevoPedidoComponent } from './nuevo-pedido.component';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import {MatIconModule} from '@angular/material/icon';
import {MatCheckboxModule} from '@angular/material/checkbox';

const routes = [
    {
        path     : 'pedidos/nuevo-pedido',
        component: NuevoPedidoComponent
    }
];

@NgModule({
    declarations: [
        NuevoPedidoComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule,
        MatListModule,
        MatCardModule,
        MatProgressBarModule,
        MatButtonModule,
        MatInputModule,
        GooglePlaceModule,
        MatIconModule,
        MatCheckboxModule
    ],
    exports     : [
        NuevoPedidoComponent
    ]
})

export class NuevoPedidoModule
{
}
